/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers;

import android.annotation.SuppressLint;
import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;

import com.aurora.wallpapers.event.Event;
import com.aurora.wallpapers.event.RxBus;
import com.aurora.wallpapers.manager.FavouritesManager;
import com.github.piasy.biv.BigImageViewer;
import com.github.piasy.biv.loader.glide.GlideImageLoader;
import com.jakewharton.rxrelay2.Relay;

import java.util.ArrayList;

import glimpse.core.Glimpse;

public class AuroraApplication extends Application {

    @SuppressLint("StaticFieldLeak")
    private static FavouritesManager favouritesManager;
    private static RxBus rxBus = null;

    public static FavouritesManager getFavouritesManager() {
        return favouritesManager;
    }

    public static Relay<Event> getRelayBus() {
        return rxBus.getBus();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Glimpse.init(this);
        BigImageViewer.initialize(GlideImageLoader.with(this));

        rxBus = new RxBus();
        favouritesManager = new FavouritesManager(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            final ArrayList<NotificationChannel> channels = new ArrayList<>();

            channels.add(new NotificationChannel(
                    Constants.NOTIFICATION_CHANNEL_WALLPAPER,
                    getString(R.string.notification_channel_updates),
                    NotificationManager.IMPORTANCE_MIN));

            final NotificationManager notificationManager = getSystemService(NotificationManager.class);
            if (notificationManager != null) {
                notificationManager.createNotificationChannels(channels);
            }
        }
    }
}
