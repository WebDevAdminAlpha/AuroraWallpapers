/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.model.fastitems;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.graphics.ColorUtils;

import com.aurora.wallpapers.AuroraApplication;
import com.aurora.wallpapers.GlideApp;
import com.aurora.wallpapers.R;
import com.aurora.wallpapers.model.DisplayMetric;
import com.aurora.wallpapers.model.Wall;
import com.aurora.wallpapers.utils.ColorUtil;
import com.aurora.wallpapers.utils.Util;
import com.bumptech.glide.request.RequestOptions;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.items.AbstractItem;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WallItem extends AbstractItem<WallItem.ViewHolder> {

    private Wall wall;

    public WallItem(Wall wall) {
        this.wall = wall;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.item_wall_thumb;
    }

    @NotNull
    @Override
    public ViewHolder getViewHolder(@NotNull View view) {
        final int height = Resources.getSystem().getDisplayMetrics().heightPixels;
        final DisplayMetric displayMetric = new DisplayMetric();
        if (displayMetric.getWidth() > displayMetric.getHeight())
            view.getLayoutParams().height = height / 2;
        else
            view.getLayoutParams().height = height / 4;
        return new ViewHolder(view);
    }

    @Override
    public int getType() {
        return R.id.fastadapter_item;
    }

    public static class ViewHolder extends FastAdapter.ViewHolder<WallItem> {
        @BindView(R.id.img)
        ImageView img;
        @BindView(R.id.img_download)
        ImageView imgFav;
        @BindView(R.id.line1)
        TextView line1;
        @BindView(R.id.line2)
        TextView line2;
        @BindView(R.id.layout_bottom)
        RelativeLayout relativeLayout;

        private Context context;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.context = itemView.getContext();
        }

        @Override
        public void bindView(@NotNull WallItem item, @NotNull List<?> list) {
            final Wall wall = item.getWall();
            final boolean isFav = Util.isFave(wall.getId());

            line1.setText(wall.getResolution());
            line2.setText(StringUtils.joinWith(StringUtils.SPACE, "Views", wall.getViews()));

            imgFav.setImageDrawable(context.getResources().getDrawable(isFav
                    ? R.drawable.ic_fav_fill
                    : R.drawable.ic_fav_line));

            imgFav.setOnClickListener(v -> {
                if (Util.isFave(wall.getId())) {
                    AuroraApplication.getFavouritesManager().removeFromWallMap(wall);
                    imgFav.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_fav_line));
                } else {
                    AuroraApplication.getFavouritesManager().addToWallMap(wall);
                    imgFav.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_fav_fill));
                }
            });

            GlideApp.with(context)
                    .asBitmap()
                    .load(wall.getThumbs().getSmall())
                    .apply(new RequestOptions().centerCrop())
                    .into(img);

            if (wall.getColors() != null && !wall.getColors().isEmpty()) {
                int backgroundColor = Color.parseColor(wall.getColors().get(0));
                int textColor = Color.WHITE;

                if (ColorUtil.isColorLight(backgroundColor)) {
                    textColor = ColorUtil.manipulateColor(backgroundColor, 0.25f);
                }

                relativeLayout.setBackgroundColor(ColorUtils.setAlphaComponent(backgroundColor, 120));
                itemView.setBackgroundColor(backgroundColor);
                line1.setTextColor(textColor);
                line2.setTextColor(textColor);
                imgFav.setImageTintList(ColorStateList.valueOf(textColor));
            }
        }

        @Override
        public void unbindView(@NotNull WallItem item) {
            line1.setText(null);
            line2.setText(null);
            //GlideApp.with(context).clear(img);
            img.setImageBitmap(null);
            imgFav.setImageDrawable(null);
        }
    }
}
