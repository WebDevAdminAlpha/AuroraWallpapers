/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.model;

import android.content.Context;

import com.aurora.wallpapers.Constants;
import com.aurora.wallpapers.utils.PrefUtil;
import com.google.gson.Gson;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Filter {
    private String category;
    private String purity;

    private String sorting;
    private String order;

    private boolean minResolutionEnabled = false;
    private String minResolution = "1920x1080";

    private List<String> resolutions;
    private List<String> ratios;

    private String colors;

    private boolean anime;
    private boolean general;
    private boolean people;

    private boolean sfw;
    private boolean sketchy;
    private boolean nsfw;

    public static boolean isEnabled(Context context) {
        return PrefUtil.getBoolean(context, Constants.PREFERENCE_FILTER_ENABLED);
    }

    public static Filter getSavedFilter(Context context) {
        return new Gson().fromJson(PrefUtil.getString(context, Constants.PREFERENCE_FILTER), Filter.class);
    }

    public Filter getDefault() {
        Filter filter = new Filter();

        filter.setPeople(true);
        filter.setGeneral(true);
        filter.setAnime(false);

        filter.setSfw(true);
        filter.setSketchy(false);
        filter.setNsfw(false);

        filter.setResolutions(new ArrayList<>());
        filter.setRatios(new ArrayList<>());
        return filter;
    }

    public String getAllResolutions() {
        return StringUtils.join(getResolutions(), ",");
    }

    public String getAllRatios() {
        return StringUtils.join(getRatios(), ",");
    }

    public String getPurity() {
        return StringUtils.join(
                isSfw() ? "1" : "0",
                isSketchy() ? "1" : "0",
                isNsfw() ? "1" : "0"
        );
    }

    public String getCategory() {
        return StringUtils.join(
                isGeneral() ? "1" : "0",
                isAnime() ? "1" : "0",
                isPeople() ? "1" : "0"
        );
    }
}
