/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.model;

import com.aurora.wallpapers.model.wallhaven.Thumbs;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Wall {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("short_url")
    @Expose
    private String shortUrl;
    @SerializedName("views")
    @Expose
    private Integer views;
    @SerializedName("favorites")
    @Expose
    private Integer favorites;
    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("purity")
    @Expose
    private String purity;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("dimension_x")
    @Expose
    private Integer dimensionX;
    @SerializedName("dimension_y")
    @Expose
    private Integer dimensionY;
    @SerializedName("resolution")
    @Expose
    private String resolution;
    @SerializedName("ratio")
    @Expose
    private String ratio;
    @SerializedName("file_size")
    @Expose
    private Integer fileSize;
    @SerializedName("file_type")
    @Expose
    private String fileType;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("colors")
    @Expose
    private List<String> colors = null;
    @SerializedName("path")
    @Expose
    private String path;
    @SerializedName("thumbs")
    @Expose
    private Thumbs thumbs;
}
