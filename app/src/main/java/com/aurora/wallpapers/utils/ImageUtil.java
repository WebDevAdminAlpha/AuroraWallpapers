/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.utils;

import android.content.res.Resources;
import android.graphics.drawable.GradientDrawable;

import androidx.annotation.ColorInt;
import androidx.core.graphics.ColorUtils;

import com.aurora.wallpapers.model.wallhaven.Size;

public class ImageUtil {

    public static Size fitToWidthAndKeepRatio(int width, int height) {
        int displayWidth = Resources.getSystem().getDisplayMetrics().widthPixels;

        int fittedHeight = height;
        int fittedWidth = width;

        fittedHeight = displayWidth * fittedHeight / fittedWidth;
        fittedWidth = displayWidth;

        return new Size(fittedWidth, fittedHeight);
    }

    public static GradientDrawable getDrawable(@ColorInt int color) {
        GradientDrawable drawable = new GradientDrawable(GradientDrawable.Orientation.TL_BR,
                new int[]{color, color});
        drawable.setShape(GradientDrawable.OVAL);
        drawable.setAlpha(200);
        return drawable;
    }

    public static GradientDrawable getGradientDrawable(@ColorInt int color) {
        GradientDrawable drawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM,
                new int[]{ColorUtils.setAlphaComponent(color, 150), ColorUtils.setAlphaComponent(color, 250)});
        drawable.setShape(GradientDrawable.RECTANGLE);
        drawable.setDither(true);
        return drawable;
    }
}
