/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.utils;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.DisplayMetrics;

import androidx.preference.PreferenceManager;

import com.aurora.wallpapers.AuroraApplication;
import com.aurora.wallpapers.Constants;
import com.aurora.wallpapers.event.Event;

import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class Util {

    public static SharedPreferences getPrefs(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static boolean isFave(String wallId) {
        return AuroraApplication.getFavouritesManager().isFav(wallId);
    }

    public static int parseInt(String intAsString, int defaultValue) {
        try {
            return Integer.parseInt(intAsString);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    public static void copyToClipBoard(Context context, String dataToCopy) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("Url", dataToCopy);
        if (clipboard != null)
            clipboard.setPrimaryClip(clip);
    }

    public static int calculateNoOfColumns(Context context, float columnWidthDp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float screenWidthDp = displayMetrics.widthPixels / displayMetrics.density;
        return (int) (screenWidthDp / columnWidthDp + 0.5);
    }

    public static String humanReadableByteValue(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
        return String.format(Locale.getDefault(), "%.1f %sB",
                bytes / Math.pow(unit, exp), pre);
    }

    public static String getBingDate(String rawDateString) {
        try {
            SimpleDateFormat rawFormatter = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
            SimpleDateFormat stdFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            Date rawDate = rawFormatter.parse(rawDateString);
            return stdFormatter.format(rawDate);
        } catch (Exception e) {
            return rawDateString;
        }
    }

    public static String getBingUrl(String url) {
        return "https://www.bing.com/" + url;
    }

    public static String getExactBingUrl(String url) {
        return "https://www.bing.com/" + url + "_1080x1920.jpg";
    }

    public static String getBingFilename(String hash) {
        return hash + ".jpg";
    }

    public static long getUpdateInterval(Context context) {
        String wallpaperInterval = PrefUtil.getString(context, Constants.PREFERENCE_UPDATES_INTERVAL);
        switch (wallpaperInterval) {
            case "0":
                return TimeUnit.MINUTES.toMillis(10);
            case "1":
                return TimeUnit.MINUTES.toMillis(15);
            case "2":
                return TimeUnit.MINUTES.toMillis(30);
            case "3":
                return TimeUnit.HOURS.toMillis(1);
            case "-1":
                return -1;
            default:
                return TimeUnit.DAYS.toMillis(1);
        }
    }

    /*PREFERENCES*/
    public static boolean isAutoWallpaperEnabled(Context context) {
        return PrefUtil.getBoolean(context, Constants.PREFERENCE_AUTO_WALLPAPER);
    }

    public static boolean isGlimpseEnabled(Context context) {
        return PrefUtil.getBoolean(context, Constants.PREFERENCE_GLIMPSE_WALLPAPER);
    }

    public static boolean isCustomBingEnabled(Context context) {
        return PrefUtil.getBoolean(context, Constants.PREFERENCE_BING_CUSTOM);
    }

    public static String getCustomBingRegion(Context context) {
        String region = PrefUtil.getString(context, Constants.PREFERENCE_BING_CUSTOM_REGION);
        if (StringUtils.isEmpty(region))
            region = "en-US";
        return region;
    }

    public static String getWallHavenAPIKey(Context context) {
        String apiKey = PrefUtil.getString(context, Constants.PREFERENCE_WALLHAVEN_API);
        if (StringUtils.isEmpty(apiKey))
            apiKey = null;
        return apiKey;
    }

    public static String getAutoWallpaperSource(Context context) {
        String wallpaperSource = PrefUtil.getString(context, Constants.PREFERENCE_AUTO_WALLPAPER_SOURCE);
        if (StringUtils.isEmpty(wallpaperSource))
            wallpaperSource = "0";
        return wallpaperSource;
    }

    public static Event.Type getAutoWallpaperTarget(Context context) {
        String wallpaperSource = PrefUtil.getString(context, Constants.PREFERENCE_AUTO_WALLPAPER_TARGET);
        switch (wallpaperSource) {
            case "0":
                return Event.Type.WALLPAPER_ONLY;
            case "1":
                return Event.Type.LOCK_ONLY;
            default:
                return Event.Type.WALLPAPER_LOCK;
        }
    }
}
