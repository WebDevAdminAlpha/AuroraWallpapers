/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.task.downloader;

import com.aurora.wallpapers.model.Wall;
import com.aurora.wallpapers.retro.RetroClient;
import com.aurora.wallpapers.retro.WallHavenService;
import com.aurora.wallpapers.utils.FileUtil;

import java.io.File;
import java.io.FileOutputStream;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class WallHavenDownloader {

    private Wall wall;

    public WallHavenDownloader(Wall wall) {
        this.wall = wall;
    }

    public File getWallpaper() throws Exception {
        File file = FileUtil.getFileFromWallId(wall);
        WallHavenService downloadService = RetroClient.getInstance().create(WallHavenService.class);
        Call<ResponseBody> call = downloadService.downloadWallpaper(wall.getPath());
        Response<ResponseBody> response = call.execute();
        if (response.body() != null) {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(response.body().bytes());
            fileOutputStream.close();
        }
        return file;
    }
}
