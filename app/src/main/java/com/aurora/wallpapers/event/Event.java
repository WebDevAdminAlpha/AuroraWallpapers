/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.event;

import lombok.Data;

@Data
public class Event {

    private Type type;
    private String stringExtra;
    private int intExtra;

    public Event(Type type) {
        this.type = type;
    }

    public Event(Type type, String stringExtra) {
        this.type = type;
        this.stringExtra = stringExtra;
    }

    public Event(Type type, int intExtra) {
        this.type = type;
        this.intExtra = intExtra;
    }

    public Event(String stringExtra, int intExtra) {
        this.stringExtra = stringExtra;
        this.intExtra = intExtra;
    }

    public enum Type {
        WALLPAPER_ONLY,
        LOCK_ONLY,
        WALLPAPER_LOCK,
        CROP
    }
}
