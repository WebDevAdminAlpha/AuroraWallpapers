/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.ui.activity;

import android.app.WallpaperManager;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.SwitchPreference;

import com.aurora.wallpapers.Constants;
import com.aurora.wallpapers.R;
import com.aurora.wallpapers.receiver.IntervalReceiver;
import com.aurora.wallpapers.utils.PrefUtil;
import com.aurora.wallpapers.utils.Util;
import com.aurora.wallpapers.utils.ViewUtil;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsActivity extends BaseActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private ActionBar actionBar;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generic);
        ButterKnife.bind(this);
        setupActionBar();
        sharedPreferences = Util.getPrefs(this);
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content, new SettingsFragment())
                .commit();
    }

    @Override
    protected void onDestroy() {
        try {
            sharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
        } catch (Exception ignored) {
        }
        super.onDestroy();
    }

    private void setupActionBar() {
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.menu_settings);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        switch (key) {
            case Constants.PREFERENCE_THEME:
                ViewUtil.switchTheme(this);
                break;
            case Constants.PREFERENCE_UPDATES_INTERVAL:
            case Constants.PREFERENCE_AUTO_WALLPAPER:
                if (Util.isAutoWallpaperEnabled(this))
                    IntervalReceiver.setAutoWallpaperInterval(this, Util.getUpdateInterval(this));
                break;
        }
    }

    public static class SettingsFragment extends PreferenceFragmentCompat {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey);
        }

        @Override
        public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);

            final Preference sourceWallpaperPreference = findPreference(Constants.PREFERENCE_AUTO_WALLPAPER_SOURCE);
            assert sourceWallpaperPreference != null;
            sourceWallpaperPreference.setOnPreferenceChangeListener((preference, newValue) -> {
                String value = newValue.toString();
                if (value.equals("0")) {
                    PrefUtil.putString(requireContext(), Constants.PREFERENCE_UPDATES_INTERVAL, "4");
                }
                return true;
            });

            final Preference clearWallpaperPreference = findPreference(Constants.PREFERENCE_CLEAR_WALLPAPER);
            assert clearWallpaperPreference != null;
            clearWallpaperPreference.setOnPreferenceClickListener(preference -> {
                final WallpaperManager wallpaperManager = WallpaperManager.getInstance(requireContext());
                boolean result = false;
                wallpaperManager.forgetLoadedWallpaper();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    wallpaperManager.clearWallpaper();
                    result = true;
                } else {
                    try {
                        wallpaperManager.clear();
                        result = true;
                    } catch (IOException ignored) {
                    }
                }
                Toast.makeText(requireContext(), getString(result
                                ? R.string.action_wallpaper_clear
                                : R.string.action_wallpaper_clear_failed),
                        Toast.LENGTH_SHORT).show();
                return false;
            });

            final SwitchPreference glimpsePreference = findPreference(Constants.PREFERENCE_GLIMPSE_WALLPAPER);
            assert glimpsePreference != null;
            if (Build.CPU_ABI.equals("x86_64") || Build.CPU_ABI.equals("x86")) {
                glimpsePreference.setVisible(false);
            }
        }
    }
}