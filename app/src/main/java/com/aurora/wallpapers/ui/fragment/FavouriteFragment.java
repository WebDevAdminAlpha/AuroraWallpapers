/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aurora.wallpapers.AuroraApplication;
import com.aurora.wallpapers.Constants;
import com.aurora.wallpapers.R;
import com.aurora.wallpapers.model.DisplayMetric;
import com.aurora.wallpapers.model.Wall;
import com.aurora.wallpapers.model.fastitems.WallItem;
import com.aurora.wallpapers.model.fastitems.decor.EqualSpacingItemDecoration;
import com.aurora.wallpapers.ui.activity.DetailsActivity;
import com.aurora.wallpapers.ui.view.ViewFlipper2;
import com.aurora.wallpapers.utils.Log;
import com.aurora.wallpapers.utils.ViewUtil;
import com.google.gson.Gson;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.adapters.ItemAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;

public class FavouriteFragment extends Fragment {

    @BindView(R.id.view_flipper)
    ViewFlipper2 viewFlipper;
    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    private FastAdapter<WallItem> fastAdapter;
    private ItemAdapter<WallItem> itemAdapter;
    private CompositeDisposable disposable = new CompositeDisposable();

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favourite, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupRecycler();
        fetchData();
    }

    private void setupRecycler() {
        fastAdapter = new FastAdapter<>();
        itemAdapter = new ItemAdapter<>();

        fastAdapter.addAdapter(0, itemAdapter);

        fastAdapter.setOnClickListener((view, iAdapter, item, position) -> {
            Intent intent = new Intent(requireContext(), DetailsActivity.class);
            intent.putExtra(Constants.STRING_EXTRA, new Gson().toJson(item.getWall()));
            requireActivity().startActivity(intent, ViewUtil.getEmptyActivityBundle((AppCompatActivity) requireActivity()));
            return false;
        });

        DisplayMetric displayMetric = new DisplayMetric();
        int gridCount = 2;
        if (displayMetric.getWidth() > displayMetric.getHeight())
            gridCount = 3;

        GridLayoutManager gridLayoutManager = new GridLayoutManager(requireContext(), gridCount);

        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.addItemDecoration(new EqualSpacingItemDecoration(16));
        recyclerView.setAdapter(fastAdapter);
    }

    private void fetchData() {
        final List<Wall> wallList = AuroraApplication.getFavouritesManager().getAllWallList();
        disposable.add(Observable.fromIterable(wallList)
                .map(WallItem::new)
                .toList()
                .subscribe(wallItems -> {
                    itemAdapter.add(wallItems);
                    if (itemAdapter != null && itemAdapter.getAdapterItems().size() > 0) {
                        viewFlipper.switchState(ViewFlipper2.DATA);
                    } else {
                        viewFlipper.switchState(ViewFlipper2.EMPTY);
                    }

                }, throwable -> Log.e(throwable.getMessage())));
    }
}
